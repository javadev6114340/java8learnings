package barkha.functional.demo;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class SupplierDemo  {
    /*
    * in java 8 if you are doing some operation like filter and you want to return
    * some dummy data or useful response use Supplier interface*/
   /*
   implements Supplier<String>
   @Override
    public String get() {
        return "Hi Java";
    }*/

    public static void  main(String[] args){
       // Supplier<String> supplier=new SupplierDemo();
         Supplier<String> supplier= () -> {
             return "Hi Java";
         };
         System.out.println(supplier.get());
        //supplier.get();

        List<String> list = Arrays.asList("1","2","3","4","5","6");
        System.out.println(list.stream().findAny().orElseGet(supplier));
    }
}
