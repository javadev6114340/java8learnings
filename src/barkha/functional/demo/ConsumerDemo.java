package barkha.functional.demo;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ConsumerDemo  {
    /*
    implements Consumer<Integer>
    * 1. Consumer is an functional interface that contains accept(T t) where T is type generic it can be
    * integer,string or other
    * 2.It takes input bur does not provide any result
    * 3. We use consumer when we need to do some operations but does not need any result
    * 4. here i am writing the lanbda expression so  do not need to implement the Consumer interface
    * and also do not need to override the accept method
     5. forEach() method in java 8 accept consumer interface*/

   /* @Override
    public void accept(Integer integer) {
        System.out.println("Printing  :: "+integer);
    }*/
    public static void main(String[] args){
        Consumer<Integer> consumer=(t) ->System.out.println("i am consumer interface : "+t);
        consumer.accept(200);

        List<Integer> list = Arrays.asList(1,2,3,4,5,6);
        list.forEach(consumer);//instead of passing reference we can also write lambda of it
        list.stream().forEach(t ->System.out.println("i am consumer with lamda : "+t));//passing lambda
        list.stream().forEach(System.out::println);//passing method reference
    }


}
