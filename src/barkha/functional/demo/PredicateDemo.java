package barkha.functional.demo;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class PredicateDemo  {

    /*
    implements Predicate<Integer>
    * 1. Here we are going to know how predicate is working
    * 2. We use predicate if we want to perform some operation and wants result. for conditional check
    * 3. Now that we implemented Predicate in traditional way
    * 4. I will also implement using lambda
    * 5. filter method in streams uses Predicate as argument */
   /* @Override
    public boolean test(Integer integer) {
        if(integer%2==0){
            return true;
        }
        return false;
    }*/

    public static void  main(String[] args){
     /* One way to write-->
       Predicate<Integer> demo =t->{
            if(t%2==0){
                return true;
            }
            return false;
        };*/
        //another way : that's how this will get passed as argument
        Predicate<Integer> demo =t->  t%2==0;
        //condition checking using if else
        if(demo.test(22784)){
            System.out.println("Even number");
        }else{
            System.out.println("Odd number");
        }
        //another way to check conditions using switch statements
        // --but switch does not take boolean argument

        //lets see how we can pass lambda of predicate in filter
        List<Integer> list = Arrays.asList(1,2,3,4,5,6);
        list.stream().filter(t->t%2==0).forEach(e -> System.out.println(e));
        list.stream().filter(t->t%2==0).forEach(System.out::println);
    }
}
