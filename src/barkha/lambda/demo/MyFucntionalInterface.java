package barkha.lambda.demo;
@FunctionalInterface
public interface MyFucntionalInterface {

    /*
   1. Functional interface in java are those interfaces which contains onle one abstract method
   2. If an interface contains more abstract method then its just a normal interface
   3. @FuntionalInterface annotation is not neccessary as long as you create only one
       abstract method it is a functional interface
   4. Before java 8 there were some functional interfaces like
          -- Runnable :contains only one method run()
          -- Callable : contains only one method call()
          -- Comparator : contains only one method compare(T o1,T o2)
          -- Comparable : contains only one method compareTo(T o1)
   5. After java 8 a functional interface can contain Defualt and static methods
   6. Some functional interfaces in java 8 are
          --Funtion<T,R>
          --Predicate<T>
          --Supplier<T>
          --Consumer<T>
   7. Only abstract method can be used as lambda expression
   8. Only functional interfaces method can be converted into anonymous function i.e lambda
    * */
    void m1();
    default  void m2(){
        System.out.println("This is default m1");
    }

    default  void m3(){
        System.out.println("This is default m3");
    }
    static  void m4(){
        System.out.println("This is static m4");
    }

}
