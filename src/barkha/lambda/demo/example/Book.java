package barkha.lambda.demo.example;

public class Book {
    private int Id;
    private String name;

    public int getId() {
        return Id;
    }

    public Book(int id, String name, double price) {
        Id = id;
        this.name = name;
        this.price = price;
    }

    public void setId(int id) {
        Id = id;
    }

    @Override
    public String toString() {
        return "Book{" +
                "Id=" + Id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    private double price;
}
