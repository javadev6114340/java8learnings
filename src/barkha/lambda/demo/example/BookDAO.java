package barkha.lambda.demo.example;

import java.util.ArrayList;
import java.util.List;

public class BookDAO {
    public List<Book> getBooks() {
        List<Book> books =new ArrayList<>();
        books.add(new Book(101,"Harry Potter",400.00));
        books.add(new Book(102,"Two Towers",300.01));
        books.add(new Book(103,"Murder Mystery",100.09));
        books.add(new Book(104,"Twilight",200.60));
        return books;
    }


}
