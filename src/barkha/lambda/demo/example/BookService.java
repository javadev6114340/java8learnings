package barkha.lambda.demo.example;

import java.util.Collections;
import java.util.List;

public class BookService {
    /*
    * 1.we are using the tradition approach to sort the books by name
    * 2.if we want to do it using lamda we dont have create a class using comparator
    * 3. instead in  Collections.sort(bookList,(o1,o2->o1.getName().compareTo(o2.getName()));
    * */
 public List<Book> getBooksInSort(){
     List<Book> bookList = new BookDAO().getBooks();
   //  Collections.sort(bookList,new BookNameComparator());
     Collections.sort(bookList,(o1,o2)->o1.getName().compareTo(o2.getName()));
     return bookList;
 }
 public static void  main(String[] args){
     System.out.println(new BookService().getBooksInSort());
 }
}
