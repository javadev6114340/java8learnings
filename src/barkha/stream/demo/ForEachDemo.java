package barkha.stream.demo;

import java.util.ArrayList;
import java.util.List;

public class ForEachDemo {

    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        list.add("Durga");
        list.add("Laxmi");
        list.add("Parvati");
        list.add("Kamla");
        list.add("Bhavani");
        //before java 8
        for( String name:list){
            System.out.println(name);
        }
        //After java 8 :two ways and
        // forEach takes Consumer interface as argument :contains method void accept(T t){}
        list.stream().forEach(t->System.out.println(t));
        list.stream().forEach(System.out::println);
    }
}
